var scene,camera,renderer/*,controls*/;
var ultiTiempo;
var labels = [];
var objetos = {};
var objetos1 = {};
var objetos2 = {};
var appW = window.innerWidth;
var appH = window.innerHeight;
var clock;


function webGLStart(){

	clock = new THREE.Clock;

	iniciarEscena();
	llenarEscena();
	ultiTiempo = Date.now();
	animarEscena();
}

function iniciarEscena(){
	renderer = new THREE.WebGLRenderer( {antialias: true} );
	renderer.setSize( appW , appH);
	renderer.setClearColorHex( 0xAAAAAA, 1.0);

	renderer.shadowMapEnabled = true;

	document.body.appendChild(renderer.domElement);


	//CAMARAS
	var view_angle = 45, aspect_ratio = appW/appH, near = 1, far = 2000;
	camera = new THREE.PerspectiveCamera( view_angle, aspect_ratio, near, far);
	camera.position.set( 0,150,400 );

	//CONTROL DE LA CAMARA
	controlCamara = new THREE.OrbitControls( camera , renderer.domElement);


	/*controls = new THREE.FirstPersonControls( camera );

				controls.movementSpeed = 500;
				controls.lookSpeed = 0.125;
				controls.lookVertical = true;
				controls.constrainVertical = true;
				controls.verticalMin = 1.1;
				controls.verticalMax = 2.2;*/


	//ESTADÍSTICAS
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '10px';
	stats.domElement.style.zIndex = '100';
	document.body.appendChild( stats.domElement );

//CANVAS GUI
	//initGUI();
//window.addEventListener( 'resize', onWindowResize, false );


	//LUZ
	//ambientLight = new THREE.AmbientLight( 0x000000 );

	luzSpotLight = new THREE.SpotLight(0xffffff);
	luzSpotLight.position.set(0,300,0);

	//configuraciones adicionales de la luz para su intensidad y sombreado
	luzSpotLight.shadowCameraVisible = false; // Guias de luz

	luzSpotLight.shadowDarkness = 0.6; // Opacidad de sombras

	luzSpotLight.intensity = 2.5; // Intensidad de Luz
	luzSpotLight.castShadow = true; // Castea Sombras?

	//Shadow map texture width in pixels.
	luzSpotLight.shadowMapWidth = luzSpotLight.shadowMapHeight = 512;

	luzSpotLight.shadowCameraNear = 50;
	luzSpotLight.shadowCameraFar = 800;
	luzSpotLight.shadowCameraFov = 50;

	//LUZ2
	//ambientLight2 = new THREE.AmbientLight( 0x000000 );

	luzSpotLight2 = new THREE.SpotLight(0xFFFFFF);
	luzSpotLight2.position.set(0,300,0);

	//configuraciones adicionales de la luz para su intensidad y sombreado
	luzSpotLight2.shadowCameraVisible = false; // Guias de luz

	luzSpotLight2.shadowDarkness = 0.8; // Opacidad de sombras

	luzSpotLight2.intensity = 2.5; // Intensidad de Luz
	luzSpotLight2.castShadow = true; // Castea Sombras?

	//Shadow map texture width in pixels.
	//luzSpotLight2.shadowMapWidth = luzSpotLight.shadowMapHeight = 512;

	luzSpotLight2.shadowCameraNear = 50;
	luzSpotLight2.shadowCameraFar = 800;
	luzSpotLight2.shadowCameraFov = 50;

	//LUZ3
	//ambientLight3 = new THREE.AmbientLight( 0x000000 );

	luzSpotLight3 = new THREE.SpotLight(0xFFFFFF);
	luzSpotLight3.position.set(0,300,0);

	//configuraciones adicionales de la luz para su intensidad y sombreado
	luzSpotLight3.shadowCameraVisible = false; // Guias de luz

	luzSpotLight3.shadowDarkness = 0.6; // Opacidad de sombras

	luzSpotLight3.intensity = 2.5; // Intensidad de Luz
	luzSpotLight3.castShadow = true; // Castea Sombras?

	//Shadow map texture width in pixels.
	//luzSpotLight2.shadowMapWidth = luzSpotLight.shadowMapHeight = 512;

	luzSpotLight3.shadowCameraNear = 50;
	luzSpotLight3.shadowCameraFar = 800;
	luzSpotLight3.shadowCameraFov = 50;

	//LUZ4
	//ambientLight4 = new THREE.AmbientLight( 0x000000 );

	luzSpotLight4 = new THREE.SpotLight(0xFFFFFF);
	luzSpotLight4.position.set(0,300,0);

	//configuraciones adicionales de la luz para su intensidad y sombreado
	luzSpotLight4.shadowCameraVisible = false; // Guias de luz

	luzSpotLight4.shadowDarkness = 0.6; // Opacidad de sombras

	luzSpotLight4.intensity = 2.5; // Intensidad de Luz
	luzSpotLight4.castShadow = true; // Castea Sombras?

	//Shadow map texture width in pixels.
	//luzSpotLight2.shadowMapWidth = luzSpotLight.shadowMapHeight = 512;

	luzSpotLight4.shadowCameraNear = 50;
	luzSpotLight4.shadowCameraFar = 800;
	luzSpotLight4.shadowCameraFov = 50;


	//Happy luces







	
	//SUELO
	var floorTexture = new THREE.ImageUtils.loadTexture( 'textures/piso.jpg');

	floorTexture.wrapS = floorTexture.wrapT = THREE.MirroredRepeatWrapping;
	floorTexture.repeat.set( 1, 1 );
	var floorMaterial = new THREE.MeshBasicMaterial( { map: floorTexture, side: THREE.DoubleSide } );
	var floorGeometry = new THREE.PlaneGeometry(1000, 1000, 10, 10);
	floor = new THREE.Mesh(floorGeometry, floorMaterial);
	//Se baja el suelo 0.5 unidades para que los objetos se situen sobre el
	floor.position.y = -0.5;
	//Aplicar la rotación para acostar el suelo
	floor.rotation.x = Math.PI / 2;

	//Activamos la recepción de sombras sobre el suelo, para esto tambien se debe activar la emisión de sombras de los elementos
	floor.receiveShadow = true;


	var puertaSuspensoTexture = new THREE.ImageUtils.loadTexture ('textures/Puerta.jpg');
	puertaSuspensoTexture.wrapS = puertaSuspensoTexture.wrapT = THREE.MirroredRepeatWrapping;
	puertaSuspensoTexture.repeat.set( 1, 1 );
	var puertaSuspensoGeom = new THREE.BoxGeometry ( 100, 200, 10, 4, 4, 4 );

	var puertaSuspensoMaterial = new THREE.MeshLambertMaterial({  map: puertaSuspensoTexture, side: THREE.DoubleSide, transparent:true ,opacity:1  });
	puertaSuspenso = new THREE.Mesh (puertaSuspensoGeom,puertaSuspensoMaterial);
	puertaSuspenso.position.set(250,100,-460);

	puertaSuspenso.rotation.y = Math.PI / -4;

	//las 3 paredes Derecha Sup
	var pared1Texture = new THREE.ImageUtils.loadTexture ('textures/ParedPrueba.jpg');
	pared1Texture.wrapS = pared1Texture.wrapT = THREE.MirroredRepeatWrapping;
	pared1Texture.repeat.set( 1, 1 );
	var paredGeom1 = new THREE.BoxGeometry ( 10, 200, 500, 2, 2, 2 );
	var paredMaterial1 = new THREE.MeshLambertMaterial({  map: pared1Texture });
	pared1 = new THREE.Mesh (paredGeom1,paredMaterial1);
	pared1.position.set(500,100,-250);
	pared1.receiveShadow = true;

	var pared2Texture = new THREE.ImageUtils.loadTexture ('textures/ParedPrueba.jpg');
	pared2Texture.wrapS = pared2Texture.wrapT = THREE.MirroredRepeatWrapping;
	pared2Texture.repeat.set( 1, 1 );
	var paredGeom2 = new THREE.BoxGeometry ( 500, 200, 10, 2, 2, 2 );
	var paredMaterial2 = new THREE.MeshLambertMaterial({  map: pared2Texture});
	pared2 = new THREE.Mesh (paredGeom2,paredMaterial2);
	pared2.position.set(250,100,-500);
	pared2.receiveShadow = true;
	
	var pared3Texture = new THREE.ImageUtils.loadTexture ('textures/ParedPrueba.jpg');
	pared3Texture.wrapS = pared3Texture.wrapT = THREE.MirroredRepeatWrapping;
	pared3Texture.repeat.set( 1, 1 );
	var paredGeom3 = new THREE.BoxGeometry ( 10, 200, 250, 2, 2, 2 );
	var paredMaterial3 = new THREE.MeshLambertMaterial({  map: pared3Texture });
	pared3 = new THREE.Mesh (paredGeom3,paredMaterial3);
	pared3.position.set(1,100,-375);
	pared3.receiveShadow = true;

	var pared4DSTexture = new THREE.ImageUtils.loadTexture ('textures/ParedPrueba.jpg');
	pared4DSTexture.wrapS = pared4DSTexture.wrapT = THREE.MirroredRepeatWrapping;
	pared4DSTexture.repeat.set(1,1);
	var pared4DSGeom = new THREE.BoxGeometry ( 250, 200, 10, 2, 2, 2 );
	var pared4DSMaterial = new THREE.MeshLambertMaterial ({map: pared4DSTexture });
	pared4DS = new THREE.Mesh (pared4DSGeom,pared4DSMaterial);
	pared4DS.position.set(375,100,1);
	pared4DS.receiveShadow = true;
	
	//Las 3 paredes Izq Sup
	var pared4Texture = new THREE.ImageUtils.loadTexture ('textures/paredromantica.jpg');
	pared4Texture.wrapS = pared1Texture.wrapT = THREE.MirroredRepeatWrapping;
	pared4Texture.repeat.set( 1, 1 );
	var paredGeom4 = new THREE.BoxGeometry ( 10, 200, 500, 2, 2, 2 );
	var paredMaterial4 = new THREE.MeshLambertMaterial({  map: pared4Texture });
	pared4 = new THREE.Mesh (paredGeom4,paredMaterial4);
	pared4.position.set(-500,100,-250);
	pared4.receiveShadow = true;
	
	var pared5Texture = new THREE.ImageUtils.loadTexture ('textures/paredromantica.jpg');
	pared5Texture.wrapS = pared2Texture.wrapT = THREE.MirroredRepeatWrapping;
	pared5Texture.repeat.set( 1, 1 );
	var paredGeom5 = new THREE.BoxGeometry ( 500, 200, 10, 2, 2, 2 );
	var paredMaterial5= new THREE.MeshLambertMaterial({  map: pared5Texture});
	pared5= new THREE.Mesh (paredGeom5,paredMaterial5);
	pared5.position.set(-250,100,-500);
	pared5.receiveShadow = true;
	
	var pared6Texture = new THREE.ImageUtils.loadTexture ('textures/paredromantica.jpg');
	pared6Texture.wrapS = pared3Texture.wrapT = THREE.MirroredRepeatWrapping;
	pared6Texture.repeat.set( 1, 1 );
	var paredGeom6 = new THREE.BoxGeometry ( 10, 200, 250, 2, 2, 2 );
	var paredMaterial6 = new THREE.MeshLambertMaterial({  map: pared6Texture });
	pared6 = new THREE.Mesh (paredGeom6,paredMaterial6);
	pared6.position.set(-1,100,-375);
	pared6.receiveShadow = true;

	var pared4ISTexture = new THREE.ImageUtils.loadTexture ('textures/paredromantica.jpg');
	pared4ISTexture.wrapS = pared4ISTexture.wrapT = THREE.MirroredRepeatWrapping;
	pared4ISTexture.repeat.set(1,1);
	var pared4ISGeom = new THREE.BoxGeometry ( 250, 200, 10, 2, 2, 2 );
	var pared4ISMaterial = new THREE.MeshLambertMaterial ({map: pared4ISTexture });
	pared4IS = new THREE.Mesh (pared4ISGeom,pared4ISMaterial);
	pared4IS.position.set(-375,100,1);
	pared4IS.receiveShadow = true;
	
	//Las 3 paredes izq Inf
	var pared7Texture = new THREE.ImageUtils.loadTexture ('textures/madera.jpg');
	pared7Texture.wrapS = pared7Texture.wrapT = THREE.MirroredRepeatWrapping;
	pared7Texture.repeat.set( 1, 1 );
	var paredGeom7 = new THREE.BoxGeometry ( 10, 200, 500, 2, 2, 2 );
	var paredMaterial7 = new THREE.MeshLambertMaterial({  map: pared7Texture });
	pared7 = new THREE.Mesh (paredGeom7,paredMaterial7);
	pared7.position.set(-500,100,250);
	pared7.receiveShadow = true;
	
	var pared8Texture = new THREE.ImageUtils.loadTexture ('textures/madera.jpg');
	pared8Texture.wrapS = pared8Texture.wrapT = THREE.MirroredRepeatWrapping;
	pared8Texture.repeat.set( 1, 1 );
	var paredGeom8 = new THREE.BoxGeometry ( 500, 200, 10, 2, 2, 2 );
	var paredMaterial8= new THREE.MeshLambertMaterial({  map: pared8Texture});
	pared8= new THREE.Mesh (paredGeom8,paredMaterial8);
	pared8.position.set(-250,100,500);
	pared8.receiveShadow = true;
	
	var pared9Texture = new THREE.ImageUtils.loadTexture ('textures/madera.jpg');
	pared9Texture.wrapS = pared9Texture.wrapT = THREE.MirroredRepeatWrapping;
	pared9Texture.repeat.set( 1, 1 );
	var paredGeom9 = new THREE.BoxGeometry ( 10, 200, 250, 2, 2, 2 );
	var paredMaterial9 = new THREE.MeshLambertMaterial({  map: pared9Texture });
	pared9 = new THREE.Mesh (paredGeom9,paredMaterial9);
	pared9.position.set(-1,100,375);
	pared9.receiveShadow = true;

	var pared4IITexture = new THREE.ImageUtils.loadTexture ('textures/madera.jpg');
	pared4IITexture.wrapS = pared4IITexture.wrapT = THREE.MirroredRepeatWrapping;
	pared4IITexture.repeat.set(1,1);
	var pared4IIGeom = new THREE.BoxGeometry ( 250, 200, 10, 2, 2, 2 );
	var pared4IIMaterial = new THREE.MeshLambertMaterial ({map: pared4IITexture });
	pared4II = new THREE.Mesh (pared4IIGeom,pared4IIMaterial);
	pared4II.position.set(-375,100,-1);
	pared4II.receiveShadow = true;

	//3 paredes inf der
	var pared10Texture = new THREE.ImageUtils.loadTexture ('textures/paredhappy.jpg');
	pared10Texture.wrapS = pared10Texture.wrapT = THREE.MirroredRepeatWrapping;
	pared10Texture.repeat.set( 1, 1 );
	var paredGeom10 = new THREE.BoxGeometry ( 10, 200, 500, 2, 2, 2 );
	var paredMaterial10 = new THREE.MeshLambertMaterial({  map: pared10Texture });
	pared10 = new THREE.Mesh (paredGeom10,paredMaterial10);
	pared10.position.set(500,100,250);
	pared10.receiveShadow = true;
	
	var pared11Texture = new THREE.ImageUtils.loadTexture ('textures/paredhappy.jpg');
	pared11Texture.wrapS = pared11Texture.wrapT = THREE.MirroredRepeatWrapping;
	pared11Texture.repeat.set( 1, 1 );
	var paredGeom11 = new THREE.BoxGeometry ( 500, 200, 10, 2, 2, 2 );
	var paredMaterial11 = new THREE.MeshLambertMaterial({  map: pared11Texture});
	pared11 = new THREE.Mesh (paredGeom11,paredMaterial11);
	pared11.position.set(250,100,500);
	pared11.receiveShadow = true;

	var pared12Texture = new THREE.ImageUtils.loadTexture ('textures/paredhappy.jpg');
	pared12Texture.wrapS = pared12Texture.wrapT = THREE.MirroredRepeatWrapping;
	pared12Texture.repeat.set( 1, 1 );
	var paredGeom12 = new THREE.BoxGeometry ( 10, 200, 250, 2, 2, 2 );
	var paredMaterial12 = new THREE.MeshLambertMaterial({  map: pared12Texture });
	pared12 = new THREE.Mesh (paredGeom3,paredMaterial3);
	pared12.position.set(1,100,375);
	pared12.receiveShadow = true;

	var pared4DITexture = new THREE.ImageUtils.loadTexture ('textures/paredhappy.jpg');
	pared4DITexture.wrapS = pared4DITexture.wrapT = THREE.MirroredRepeatWrapping;
	pared4DITexture.repeat.set(1,1);
	var pared4DIGeom = new THREE.BoxGeometry ( 250, 200, 10, 2, 2, 2 );
	var pared4DIMaterial = new THREE.MeshLambertMaterial ({map: pared4DITexture });
	pared4DI = new THREE.Mesh (pared4DIGeom,pared4DIMaterial);
	pared4DI.position.set(375,100,1);
	pared4DI.receiveShadow = true;










//Objetos Romance
var cajaDeRegaloTexture = new THREE.ImageUtils.loadTexture ('textures/papelregalo.jpg');
cajaDeRegaloTexture.wrapS = cajaDeRegaloTexture.wrapT = THREE.MirroredRepeatWrapping;
cajaDeRegaloTexture.repeat.set(1,1);
var cajaDeRegaloGeom = new THREE.BoxGeometry ( 75, 5, 50, 50, 50, 50 );
var cajaDeRegaloMaterial = new THREE.MeshLambertMaterial ({map: cajaDeRegaloTexture });
cajaDeRegalo = new THREE.Mesh (cajaDeRegaloGeom,cajaDeRegaloMaterial);
cajaDeRegalo.position.set(-250,50,-250);
cajaDeRegalo.receiveShadow = true;
cajaDeRegalo.rotation.y = Math.PI /-4;
cajaDeRegalo.castShadow = true;

var mesaRomanceGeom = new THREE.BoxGeometry ( 150, 50, 100, 50, 50, 50 );
var mesaRomanceMaterial = new THREE.MeshLambertMaterial ({color: 0xDDA0DD, transparent:true, opacity:1});
mesaRomance = new THREE.Mesh (mesaRomanceGeom,mesaRomanceMaterial);
mesaRomance.position.set(-250,25,-250);
mesaRomance.receiveShadow = true;
mesaRomance.castShadow = true;


var sphereGeom =  new THREE.SphereGeometry( 25, 10, 16 );

var sphereTextureL = new THREE.ImageUtils.loadTexture ('textures/L.jpg');
sphereTextureL.wrapS = sphereTextureL.wrapT = THREE.MirroredRepeatWrapping;
sphereTextureL.repeat.set( 1, 1 );
var sphereTextureO = new THREE.ImageUtils.loadTexture ('textures/O.jpg');
sphereTextureO.wrapS = sphereTextureO.wrapT = THREE.MirroredRepeatWrapping;
sphereTextureO.repeat.set( 1, 1 );
var sphereTextureV = new THREE.ImageUtils.loadTexture ('textures/V.jpg');
sphereTextureV.wrapS = sphereTextureV.wrapT = THREE.MirroredRepeatWrapping;
sphereTextureV.repeat.set( 1, 1 );
var sphereTextureE = new THREE.ImageUtils.loadTexture ('textures/E.jpg');
sphereTextureE.wrapS = sphereTextureE.wrapT = THREE.MirroredRepeatWrapping;
sphereTextureE.repeat.set( 1, 1 );


var darkMateriaL = new THREE.MeshPhongMaterial( {  transparent:true, opacity:1, map: sphereTextureL } );
var darkMateriaO = new THREE.MeshPhongMaterial( {  transparent:true, opacity:1, map: sphereTextureO } );
var darkMateriaV = new THREE.MeshPhongMaterial( {  transparent:true, opacity:1, map: sphereTextureV } );
var darkMateriaE = new THREE.MeshPhongMaterial( {  transparent:true, opacity:1, map: sphereTextureE } );

var sphere = new THREE.Mesh( sphereGeom.clone(), darkMateriaL );
sphere.position.set(-320, 100, -350);
objetos.sphere1= sphere;	
var sphere = new THREE.Mesh( sphereGeom.clone(), darkMateriaO );
sphere.position.set(-270, 100, -300);
objetos.sphere2= sphere;	
var sphere = new THREE.Mesh( sphereGeom.clone(), darkMateriaV );
sphere.position.set(-215, 100, -350);
objetos.sphere3= sphere;	
var sphere = new THREE.Mesh( sphereGeom.clone(), darkMateriaE );
sphere.position.set(-160, 100, -300);
objetos.sphere4= sphere;	
objetos.sphere1.rotation.y = Math.PI / -2;
objetos.sphere2.rotation.y = Math.PI / -2;
objetos.sphere3.rotation.y = Math.PI / -2;
objetos.sphere4.rotation.y = Math.PI / -2;

var cuadroRomanceTexture = new THREE.ImageUtils.loadTexture ('textures/cuadroromance.jpg');
cuadroRomanceTexture.wrapS = pared2Texture.wrapT = THREE.MirroredRepeatWrapping;
cuadroRomanceTexture.repeat.set( 1, 1 );
var cuadroRomanceGeometry = new THREE.BoxGeometry ( 100, 100, 10, 2, 2, 2 );
var cuadroRomanceMaterial= new THREE.MeshLambertMaterial({  map: cuadroRomanceTexture});
cuadroRomance= new THREE.Mesh (cuadroRomanceGeometry,cuadroRomanceMaterial);
cuadroRomance.position.set(-250,100,-495);
cuadroRomance.receiveShadow = true;






//ZEN

var piedraZenGeom =  new THREE.SphereGeometry( 25, 32, 16 );

var piedraZenTexture = new THREE.ImageUtils.loadTexture ('textures/rocazen.jpg');
piedraZenTexture.wrapS = piedraZenTexture.wrapT = THREE.MirroredRepeatWrapping;
piedraZenTexture.repeat.set( 1, 1 );


var piedraZenMaterial = new THREE.MeshLambertMaterial( {  transparent:true, opacity:1, map: piedraZenTexture } );

var piedraZen = new THREE.Mesh( piedraZenGeom.clone(), piedraZenMaterial );
piedraZen.position.set(-330, 25, 400);
objetos1.sphere1= piedraZen;	
var piedraZen = new THREE.Mesh( piedraZenGeom.clone(), piedraZenMaterial );
piedraZen.position.set(-270, 25, 400);
objetos1.sphere2= piedraZen;	
var piedraZen = new THREE.Mesh( piedraZenGeom.clone(), piedraZenMaterial );
piedraZen.position.set(-215, 25, 400);
objetos1.sphere3= piedraZen;	
var piedraZen = new THREE.Mesh( piedraZenGeom.clone(), piedraZenMaterial );
piedraZen.position.set(-160, 25, 400);
objetos1.sphere4= piedraZen;

var tablaZenTexture = new THREE.ImageUtils.loadTexture ('textures/maderatabla.jpg');
tablaZenTexture.wrapS = tablaZenTexture.wrapT = THREE.MirroredRepeatWrapping;
tablaZenTexture.repeat.set(1,1);
var tablaZenGeom = new THREE.BoxGeometry ( 210, 10, 60, 50, 50, 50 );
var tablaZenMaterial = new THREE.MeshLambertMaterial ({map: tablaZenTexture });
tablaZen = new THREE.Mesh (tablaZenGeom,tablaZenMaterial);
tablaZen.position.set(-240,50,400);
tablaZen.receiveShadow = true;
tablaZen.castShadow = true;

var fuenteZenTexture = new THREE.ImageUtils.loadTexture ('textures/rocazen.jpg');
fuenteZenTexture.wrapS = fuenteZenTexture.wrapT = THREE.MirroredRepeatWrapping;
tablaZenTexture.repeat.set(1,1);
var fuenteZenGeom = new THREE.CylinderGeometry ( 100, 100, 50, 32, 3, true );
var fuenteZenMaterial = new THREE.MeshLambertMaterial ({map: fuenteZenTexture });
fuenteZen = new THREE.Mesh (fuenteZenGeom,fuenteZenMaterial);
fuenteZen.position.set(-500,25,200);
fuenteZen.receiveShadow = true;
fuenteZen.castShadow = true;

var aguaTexture = new THREE.ImageUtils.loadTexture ('textures/aguazen.jpg');
aguaTexture.wrapS = aguaTexture.wrapT = THREE.MirroredRepeatWrapping;
tablaZenTexture.repeat.set(1,1);
var aguaGeom = new THREE.CylinderGeometry ( 99, 99, 50, 32, 1, false );
var aguaMaterial = new THREE.MeshPhongMaterial ({map: aguaTexture });
aguaZen = new THREE.Mesh (aguaGeom,aguaMaterial);
aguaZen.position.set(-500,20,200);
aguaZen.receiveShadow = true;
aguaZen.castShadow = false;


var cuadroZen1Texture = new THREE.ImageUtils.loadTexture ('textures/cuadrozen1.jpg');
cuadroZen1Texture.wrapS = cuadroZen1Texture.wrapT = THREE.MirroredRepeatWrapping;
cuadroZen1Texture.repeat.set( 1, 1 );
var cuadroZen1Geometry = new THREE.BoxGeometry ( 10, 100, 70, 2, 2, 2 );
var cuadroZen1Material= new THREE.MeshLambertMaterial({  map: cuadroZen1Texture});
cuadroZen1= new THREE.Mesh (cuadroZen1Geometry,cuadroZen1Material);
cuadroZen1.position.set(-495,100,330);
cuadroZen1.receiveShadow = true;

var cuadroZen2Texture = new THREE.ImageUtils.loadTexture ('textures/cuadrozen2.jpg');
cuadroZen2Texture.wrapS = cuadroZen2Texture.wrapT = THREE.MirroredRepeatWrapping;
cuadroZen2Texture.repeat.set( 1, 1 );
var cuadroZen2Geometry = new THREE.BoxGeometry ( 10, 90, 65, 2, 2, 2 );
var cuadroZen2Material= new THREE.MeshLambertMaterial({  map: cuadroZen2Texture});
cuadroZen2= new THREE.Mesh (cuadroZen2Geometry,cuadroZen2Material);
cuadroZen2.position.set(-495,100,255);
cuadroZen2.receiveShadow = true;

var cuadroZen3Texture = new THREE.ImageUtils.loadTexture ('textures/cuadrozen3.jpg');
cuadroZen3Texture.wrapS = cuadroZen3Texture.wrapT = THREE.MirroredRepeatWrapping;
cuadroZen3Texture.repeat.set( 1, 1 );
var cuadroZen3Geometry = new THREE.BoxGeometry ( 10, 80, 60, 2, 2, 2 );
var cuadroZen3Material= new THREE.MeshLambertMaterial({  map: cuadroZen3Texture});
cuadroZen3= new THREE.Mesh (cuadroZen3Geometry,cuadroZen3Material);
cuadroZen3.position.set(-495,100,185);
cuadroZen3.receiveShadow = true;


//suspenso

var relojArribaTexture = new THREE.ImageUtils.loadTexture ('textures/relojarriba.jpg');
relojArribaTexture.wrapS = relojArribaTexture.wrapT = THREE.MirroredRepeatWrapping;
relojArribaTexture.repeat.set( 1, 1 );
var relojArribaGeometry = new THREE.BoxGeometry ( 40, 40, 40, 40, 40, 40 );
var relojArribaMaterial= new THREE.MeshLambertMaterial({  map: relojArribaTexture});
relojArriba= new THREE.Mesh (relojArribaGeometry,relojArribaMaterial);
relojArriba.position.set(470,160,-450);
relojArriba.receiveShadow = true;


var relojMedioTexture = new THREE.ImageUtils.loadTexture ('textures/relojmedio.jpg');
relojMedioTexture.wrapS = relojMedioTexture.wrapT = THREE.MirroredRepeatWrapping;
relojMedioTexture.repeat.set( 1, 1 );
var relojMedioGeometry = new THREE.BoxGeometry ( 40, 100, 40, 40, 40, 40 );
var relojMedioMaterial= new THREE.MeshLambertMaterial({  map: relojMedioTexture});
relojMedio= new THREE.Mesh (relojMedioGeometry,relojMedioMaterial);
relojMedio.position.set(470,90,-450);
relojMedio.receiveShadow = true;

var relojAbajoTexture = new THREE.ImageUtils.loadTexture ('textures/relojabajo.jpg');
relojAbajoTexture.wrapS = relojAbajoTexture.wrapT = THREE.MirroredRepeatWrapping;
relojAbajoTexture.repeat.set( 1, 1 );
var relojAbajoGeometry = new THREE.BoxGeometry ( 60, 40, 40, 40, 40, 40 );
var relojAbajoMaterial= new THREE.MeshLambertMaterial({  map: relojAbajoTexture});
relojAbajo= new THREE.Mesh (relojAbajoGeometry,relojAbajoMaterial);
relojAbajo.position.set(470,20,-450);
relojAbajo.receiveShadow = true;

relojArriba.rotation.y = Math.PI / -4;
relojMedio.rotation.y = Math.PI / -4;
relojAbajo.rotation.y = Math.PI / -4;


var TVTexture = new THREE.ImageUtils.loadTexture ('textures/tvsusp.jpg');
TVTexture.wrapS = TVTexture.wrapT = THREE.MirroredRepeatWrapping;
TVTexture.repeat.set( 1, 1 );
var TVGeometry = new THREE.BoxGeometry ( 70, 50, 50, 50, 50, 50 );
var TVMaterial= new THREE.MeshLambertMaterial({  map: TVTexture});
TVsuspenso= new THREE.Mesh (TVGeometry,TVMaterial);
TVsuspenso.position.set(205,40,-250);
TVsuspenso.receiveShadow = true;
TVsuspenso.rotation.y = Math.PI / -4;
TVsuspenso.rotation.z = Math.PI / -4;

var TVTexture1 = new THREE.ImageUtils.loadTexture ('textures/relojabajo.jpg');
TVTexture1.wrapS = TVTexture1.wrapT = THREE.MirroredRepeatWrapping;
TVTexture1.repeat.set( 1, 1 );
var TVGeometry1 = new THREE.BoxGeometry ( 71, 51, 49, 51, 51, 51 );
var TVMaterial1 = new THREE.MeshLambertMaterial({  map: TVTexture1});
TVsuspenso1= new THREE.Mesh (TVGeometry1,TVMaterial1);
TVsuspenso1.position.set(205,40,-250);
TVsuspenso1.receiveShadow = true;
TVsuspenso1.rotation.y = Math.PI / -4;
TVsuspenso1.rotation.z = Math.PI / -4;
TVsuspenso1.castShadow = true;

var mesaTvTexture = new THREE.ImageUtils.loadTexture ('textures/rocazen.jpg');
mesaTvTexture.wrapS = mesaTvTexture.wrapT = THREE.MirroredRepeatWrapping;
tablaZenTexture.repeat.set(1,1);
var mesaTvGeom = new THREE.CylinderGeometry ( 50, 50, 50, 8, 3, false );
var mesaTvMaterial = new THREE.MeshLambertMaterial ({map: mesaTvTexture });
mesaTv = new THREE.Mesh (mesaTvGeom,mesaTvMaterial);
mesaTv.position.set(130,20,-300);
mesaTv.receiveShadow = true;
mesaTv.castShadow = true;
mesaTv.rotation.y = Math.PI / -2.5;



//happy

var globoHappyGeom =  new THREE.SphereGeometry( 25, 10, 16 );

var globoHappyTexture1 = new THREE.ImageUtils.loadTexture ('textures/globo1.jpg');
globoHappyTexture1.wrapS = globoHappyTexture1.wrapT = THREE.MirroredRepeatWrapping;
globoHappyTexture1.repeat.set( 1, 1 );
var globoHappyTexture2 = new THREE.ImageUtils.loadTexture ('textures/globo2.jpg');
globoHappyTexture2.wrapS = globoHappyTexture2.wrapT = THREE.MirroredRepeatWrapping;
globoHappyTexture2.repeat.set( 1, 1 );
var globoHappyTexture3 = new THREE.ImageUtils.loadTexture ('textures/globo3.jpg');
globoHappyTexture3.wrapS = globoHappyTexture3.wrapT = THREE.MirroredRepeatWrapping;
globoHappyTexture3.repeat.set( 1, 1 );
var globoHappyTexture4 = new THREE.ImageUtils.loadTexture ('textures/globo4.jpg');
globoHappyTexture4.wrapS = globoHappyTexture4.wrapT = THREE.MirroredRepeatWrapping;
globoHappyTexture4.repeat.set( 1, 1 );


var globoHappyMaterial1 = new THREE.MeshLambertMaterial( {  transparent:true, opacity:1, map: globoHappyTexture1 } );
var globoHappyMaterial2 = new THREE.MeshLambertMaterial( {  transparent:true, opacity:1, map: globoHappyTexture2 } );
var globoHappyMaterial3 = new THREE.MeshLambertMaterial( {  transparent:true, opacity:1, map: globoHappyTexture3 } );
var globoHappyMaterial4 = new THREE.MeshLambertMaterial( {  transparent:true, opacity:1, map: globoHappyTexture4 } );


var globoHappy = new THREE.Mesh( globoHappyGeom.clone(), globoHappyMaterial1 );
globoHappy.position.set(330, 180, 400);
objetos2.sphere1= globoHappy;	
var globoHappy = new THREE.Mesh( globoHappyGeom.clone(), globoHappyMaterial2 );
globoHappy.position.set(270, 180, 400);
objetos2.sphere2= globoHappy;	
var globoHappy = new THREE.Mesh( globoHappyGeom.clone(), globoHappyMaterial3 );
globoHappy.position.set(215, 180, 400);
objetos2.sphere3= globoHappy;	
var globoHappy = new THREE.Mesh( globoHappyGeom.clone(), globoHappyMaterial4 );
globoHappy.position.set(160, 180, 400);
objetos2.sphere4= globoHappy;

objetos2.sphere1.rotation.y = Math.PI / 2;
objetos2.sphere2.rotation.y = Math.PI / 2;
objetos2.sphere3.rotation.y = Math.PI / 2;
objetos2.sphere4.rotation.y = Math.PI / 2;

var discoHappyGeom = new THREE.SphereGeometry (40, 32, 16)
var discoTexture = new THREE.ImageUtils.loadTexture ('textures/disco.jpg');
discoTexture.wrapS = discoTexture.wrapT = THREE.MirroredRepeatWrapping;
discoTexture.repeat.set( 1, 1 );

var discoMaterial = new THREE.MeshLambertMaterial( {  transparent:true, opacity:1, map: discoTexture } );

discoHappy = new THREE.Mesh(discoHappyGeom,discoMaterial);
discoHappy.position.set(250,180,250);

var sphereLuz = new THREE.SphereGeometry( 0.25, 16, 8 );
light1 = new THREE.PointLight( 0x0000FF, 2.5, 100 );
light1.add( new THREE.Mesh( sphereLuz, new THREE.MeshBasicMaterial( { color: 0x0000FF } ) ) );
light1.position.set(250,0,250);






//sonidos

sonido1 = new Sound(['sounds/romance.mp3'],250,1);
sonido1.position.set(-250,0,-250);
sonido1.play();

sonido2 = new Sound(['sounds/suspenso.mp3'],250,1);
sonido2.position.set(250,0,-250);
sonido2.play();

sonido3 = new Sound(['sounds/Relax.mp3'],250,1);
sonido3.position.set(-250,0,250);
sonido3.play();

sonido4 = new Sound(['sounds/happy.mp3'],250,1);
sonido4.position.set(250,0,250);
sonido4.play();





}







function llenarEscena() {
	scene = new THREE.Scene();
	scene.fog = new THREE.Fog( 0x808080, 1000, 6000 );
	scene.add( camera );
	camera.lookAt(scene.position);

	// Luces
	//Sin la luz ambiente no se perciben las componentes ambiente de los elementos, estas por defecto son blancas y para fines de práctica se adicionará esta luz al final del taller.
	//scene.add( ambientLight );
	scene.add( luzSpotLight );
	scene.add( luzSpotLight2 );
	scene.add( luzSpotLight3 );
	scene.add( luzSpotLight4 );
	scene.add( puertaSuspenso );
	scene.add( floor );
	scene.add( pared1 );
	scene.add( pared2 );
	scene.add( pared3 );
	scene.add( pared4 );
	scene.add( pared5 );
	scene.add( pared6 );
	scene.add( pared7 );
	scene.add( pared8 );
	scene.add( pared9 );
	scene.add( pared10 );
	scene.add( pared11 );
	scene.add( pared12 );
	scene.add( pared4DS );
	scene.add( pared4IS );
	scene.add( pared4DI );
	scene.add( cajaDeRegalo );
	scene.add(mesaRomance);
	scene.add(cuadroRomance);
	scene.add(tablaZen);
	scene.add(fuenteZen);
	scene.add(aguaZen);
	scene.add(cuadroZen1);
	scene.add(cuadroZen2);
	scene.add(cuadroZen3);
	scene.add(relojArriba);
	scene.add(relojMedio);
	scene.add(relojAbajo);
	scene.add(TVsuspenso);
	scene.add(TVsuspenso1);
	scene.add(mesaTv);
	scene.add(discoHappy);
	scene.add( light1 );

	for(var i=0; i<Object.keys(objetos).length; i++)
	{
		var object = Object.keys(objetos)[i];
		objetos[object].castShadow = true;
		objetos[object].shininess = 30;
		scene.add(objetos[object]);
	}

	for(var i=0; i<Object.keys(objetos1).length; i++)
	{
		var object1 = Object.keys(objetos1)[i];
		objetos1[object1].castShadow = true;
		objetos1[object1].shininess = 30;
		scene.add(objetos1[object1]);
	}	

	for(var i=0; i<Object.keys(objetos2).length; i++)
	{
		var object2 = Object.keys(objetos1)[i];
		objetos2[object2].castShadow = true;
		objetos2[object2].shininess = 30;
		scene.add(objetos2[object2]);
	}	

	puertaSuspenso.castShadow=true;
	
	//Niebla
	scene.fog = new THREE.FogExp2( 0x969696, 0.0025 );

	var lightTarget1 = new THREE.Object3D();
	lightTarget1.position.set(500,100,-500);
	scene.add( lightTarget1 );
	luzSpotLight2.target = lightTarget1;

	var lightTarget2 = new THREE.Object3D();
	lightTarget2.position.set(-500,100,-500);
	scene.add( lightTarget2 );
	luzSpotLight.target = lightTarget2;

	var lightTarget3 = new THREE.Object3D();
	lightTarget3.position.set(-500,100,500);
	scene.add( lightTarget3 );
	luzSpotLight3.target = lightTarget3;

	var lightTarget4 = new THREE.Object3D();
	lightTarget4.position.set(500,100,500);
	scene.add( lightTarget4 );
	luzSpotLight4.target = lightTarget4;




}

function animarEscena(){
	requestAnimationFrame( animarEscena );
	renderEscena();
	actualizarEscena();
}

function renderEscena(){
	renderer.render( scene, camera );
	//controls.update( clock.getDelta() );
}

function actualizarEscena(){
	
	ultiTiempo = Date.now();
	controlCamara.update();

	var time = Date.now() *0.0005;
	var delta = clock.getDelta();




//movimiento de los globos
objetos2.sphere1.position.x = 350+Math.sin( time * 0.4 ) * 120;
objetos2.sphere1.position.y = 100+Math.cos( time * 0.5 ) * 80;
objetos2.sphere1.position.z = 120+Math.cos( time * 1.3 ) * 80;

objetos2.sphere2.position.x = 320+Math.sin( time * 1.4 ) * 120;
objetos2.sphere2.position.y = 120+Math.cos( time * 1.5 ) * 80;
objetos2.sphere2.position.z = 200+Math.cos( time * 1.9 ) * 80;

objetos2.sphere3.position.x = 300+Math.sin( time * 1.4 ) * 120;
objetos2.sphere3.position.y = 140+Math.cos( time * 1.0 ) * 80;
objetos2.sphere3.position.z = 250+Math.cos( time * 0.3 ) * 80;

objetos2.sphere4.position.x = 250+Math.sin( time * 1.4 ) * 120;
objetos2.sphere4.position.y = 180+Math.cos( time * 0.5 ) * 80;
objetos2.sphere4.position.z = 300+Math.cos( time * 1.3 ) * 80;







stats.update();

sonido1.update(camera);
sonido2.update(camera);
sonido3.update(camera);
sonido4.update(camera);
	//luzSpotLight.intensity.update(camera);
}

/*function onWindowResize() {



				renderer.setSize( window.innerWidth, window.innerHeight );

				controls.handleResize();

			}*/

			function TextureAnimator(texture, tilesHoriz, tilesVert, numTiles, tileDispDuration) 
			{	
	// note: texture passed by reference, will be updated by the update function.

	this.tilesHorizontal = tilesHoriz;
	this.tilesVertical = tilesVert;
	// how many images does this spritesheet contain?
	//  usually equals tilesHoriz * tilesVert, but not necessarily,
	//  if there at blank tiles at the bottom of the spritesheet. 
	this.numberOfTiles = numTiles;
	texture.wrapS = texture.wrapT = THREE.RepeatWrapping; 
	texture.repeat.set( 1 / this.tilesHorizontal, 1 / this.tilesVertical );

	// how long should each image be displayed?
	this.tileDisplayDuration = tileDispDuration;

	// how long has the current image been displayed?
	this.currentDisplayTime = 0;

	// which image is currently being displayed?
	this.currentTile = 0;

	this.update = function( milliSec )
	{
		this.currentDisplayTime += milliSec;
		while (this.currentDisplayTime > this.tileDisplayDuration)
		{
			this.currentDisplayTime -= this.tileDisplayDuration;
			this.currentTile++;
			if (this.currentTile == this.numberOfTiles)
				this.currentTile = 0;
			var currentColumn = this.currentTile % this.tilesHorizontal;
			texture.offset.x = currentColumn / this.tilesHorizontal;
			var currentRow = Math.floor( this.currentTile / this.tilesHorizontal );
			texture.offset.y = currentRow / this.tilesVertical;
		}
	};
}



